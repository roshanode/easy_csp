import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:csp/AuthScreens/login_signup_page.dart';
import 'package:csp/addEvents.dart';
import 'package:csp/addIdeas.dart';
import 'package:csp/events.dart';
import 'package:csp/message.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:csp/eventDetail.dart';

import '../addCSP.dart';
import 'authentication.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key key, this.auth, this.userId, this.onSignedOut})
      : super(key: key);

  final BaseAuth auth;
  final VoidCallback onSignedOut;
  final String userId;
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  String userEmail;
  final searchController = TextEditingController();

  @override
  void initState() {
    super.initState();
    userData();
  }

  Future<String> userData() async {
    final FirebaseUser user = await FirebaseAuth.instance.currentUser();
    final String email = user.email.toString();
    this.setState(() {
      userEmail = email;
    });
    print(userEmail);
    return email;
  }

  //for sign out
  _signOut() async {
    try {
      await widget.auth.signOut();
      widget.onSignedOut();
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget _buildListItem(BuildContext context, DocumentSnapshot document,
        String collectionName) {
      return InkWell(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => EventDetail(
                      eventImage: document['EventImage'],
                      eventDate: document['EventDate'],
                      eventDescription: document['EventDescription'],
                      eventName: document['EventName'],
                      teamName: document['TeamName'],
                      comments: document['test'],
                      id: document.documentID,
                      collectionName: collectionName,
                    )),
          );
        },
        child: Container(
          margin: EdgeInsets.all(8),
          width: MediaQuery.of(context).size.width / 2.5,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10.0),
              boxShadow: [
                new BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.08),
                  blurRadius: 20.0,
                ),
              ]),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              CachedNetworkImage(
                imageUrl: document['EventImage'],
                height: 100,
                width: MediaQuery.of(context).size.width,
                fit: BoxFit.cover,
                placeholder: (context, url) => CircularProgressIndicator(),
                errorWidget: (context, url, error) => Icon(Icons.error),
              ),
              Spacer(),
              Text(
                document['EventName'],
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                ),
              ),
              Spacer(),
              Text(
                document['EventDate'],
                style: TextStyle(color: Colors.grey),
              ),
              Spacer()
            ],
          ),
        ),
      );
    }

    return Scaffold(
      //backgroundColor: Color.fromARGB(0xff, 241, 241, 254),
      appBar: AppBar(
          title: Text("HOME"),
          backgroundColor: Colors.blue[800],
          actions: <Widget>[
            Padding(
              padding: const EdgeInsets.only(right: 20.0),
              child: InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Message()),
                  );
                },
                child: Icon(Icons.email),
              ),
            )
          ],
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(40.0),
            child: Theme(
              data: Theme.of(context).copyWith(accentColor: Colors.white),
              child: Container(
                margin: EdgeInsets.only(
                  bottom: 10,
                  top: 5,
                ),
                height: 40,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  child: Form(
                    //key: _key,
                    child: TextFormField(
                        decoration: InputDecoration(
                      contentPadding: EdgeInsets.symmetric(vertical: 2),
                      prefixIcon: InkWell(
                        child: Icon(
                          Icons.search,
                          size: 20,
                          color: Color.fromRGBO(142, 142, 147, 1),
                        ),
                      ),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(18),
                        borderSide: BorderSide(
                          width: 0,
                          style: BorderStyle.none,
                        ),
                      ),
                      filled: true,
                      hintText: "What you are looking for?",
                      fillColor: Colors.white,
                    )),
                  ),
                ),
              ),
            ),
          )),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.all(8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    'Past CSP Events',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => Events(
                                  collectionName: 'pastEvents',
                                )),
                      );
                    },
                    child: Text(
                      'See More',
                      style: TextStyle(
                        color: Colors.blue,
                      ),
                    ),
                  )
                ],
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height / 3.5,
              child: StreamBuilder(
                  stream:
                      Firestore.instance.collection('pastEvents').snapshots(),
                  builder: (context, snapshot) {
                    if (!snapshot.hasData)
                      return LinearProgressIndicator(
                        backgroundColor: Colors.green,
                      );
                    return ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: snapshot.data.documents.length,
                      itemBuilder: (context, index) => _buildListItem(context,
                          snapshot.data.documents[index], 'pastEvents'),
                    );
                  }),
            ),
            Container(
              margin: EdgeInsets.all(8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    'Upcomming CSP Events',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => Events(
                                  collectionName: 'upcommingEvents',
                                )),
                      );
                    },
                    child: Text(
                      'See More',
                      style: TextStyle(
                        color: Colors.blue,
                      ),
                    ),
                  )
                ],
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height / 3.5,
              child: StreamBuilder(
                  stream: Firestore.instance
                      .collection('upcommingEvents')
                      .snapshots(),
                  builder: (context, snapshot) {
                    if (!snapshot.hasData)
                      return LinearProgressIndicator(
                        backgroundColor: Colors.green,
                      );
                    return ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: snapshot.data.documents.length,
                      itemBuilder: (context, index) => _buildListItem(context,
                          snapshot.data.documents[index], 'upcommingEvents'),
                    );
                  }),
            ),
            Container(
              margin: EdgeInsets.all(8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    'Cloud Workers Ideas',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => Events(
                                  collectionName: 'ideas',
                                )),
                      );
                    },
                    child: Text(
                      'See More',
                      style: TextStyle(
                        color: Colors.blue,
                      ),
                    ),
                  )
                ],
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height / 3.5,
              child: StreamBuilder(
                  stream: Firestore.instance.collection('ideas').snapshots(),
                  builder: (context, snapshot) {
                    if (!snapshot.hasData)
                      return LinearProgressIndicator(
                        backgroundColor: Colors.green,
                      );
                    return ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: snapshot.data.documents.length,
                      itemBuilder: (context, index) => _buildListItem(
                          context, snapshot.data.documents[index], 'ideas'),
                    );
                  }),
            ),
          ],
        ),
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Column(
                children: <Widget>[
                  CachedNetworkImage(
                    height: 80,
                    width: 80,
                    // width: MediaQuery.of(context).size.width,
                    fit: BoxFit.cover,
                    imageUrl:
                        "https://i.pinimg.com/originals/83/c0/0f/83c00f59d66869aa22d3bd5f35e26c6d.png",
                    placeholder: (context, url) => CircularProgressIndicator(),
                    errorWidget: (context, url, error) => Icon(Icons.error),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Text(
                        'Hello,\n' + userEmail,
                        style: TextStyle(color: Colors.white),
                      ),
                      IconButton(
                        icon: Icon(
                          Icons.edit,
                          size: 30,
                          color: Colors.white,
                        ),
                        onPressed: () {
                          Navigator.of(context)
                              .pushReplacementNamed('/profile');
                        },
                      ),
                    ],
                  ),
                ],
              ),
              decoration: BoxDecoration(
                color: Colors.blue[800],
              ),
            ),
            ListTile(
              title: Text('Add Upcomming Events'),
              trailing: Icon(Icons.event),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => AddEvents()),
                );
              },
            ),
            ListTile(
              title: Text('Add CSP Experience'),
              trailing: Icon(Icons.add_to_photos),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => AddCSP()),
                );
              },
            ),
            ListTile(
              title: Text('Add Ideas'),
              trailing: Icon(Icons.dock),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => AddIdeas()),
                );
              },
            ),
            ListTile(
                title: Text('Log Out'),
                trailing: Icon(Icons.exit_to_app),
                onTap: () {
                  _signOut();
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => LoginSignUpPage()),
                  );
                }),
          ],
        ),
      ),
    );
  }
}
