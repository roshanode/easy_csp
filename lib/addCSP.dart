import 'dart:io';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:path/path.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class AddCSP extends StatefulWidget {
  @override
  _AddCSPState createState() => _AddCSPState();
}

class _AddCSPState extends State<AddCSP> {
  String userEmail;

  bool isUploading = false;

  @override
  void initState() {
    super.initState();
    userData();
  }

  Future<String> userData() async {
    final FirebaseUser user = await FirebaseAuth.instance.currentUser();
    final String email = user.email.toString();
    this.setState(() {
      userEmail = email;
    });
    print(userEmail);
    return email;
  }

  final eventName = TextEditingController();
  final eventDate = TextEditingController();
  final eventDescription = TextEditingController();
  final teamName = TextEditingController();
  final databaseReference = Firestore.instance;
  String imgUrl;
  File _image;

  Future uploadPic(BuildContext context) async {
    String fileName = basename(_image.path);
    StorageReference firebaseStorageRef =
        FirebaseStorage.instance.ref().child(fileName);
    StorageUploadTask uploadTask = firebaseStorageRef.putFile(_image);
    var downUrl = await (await uploadTask.onComplete).ref.getDownloadURL();
    var url = downUrl.toString();
    setState(() {
      print("Event Picture uploaded");
      imgUrl = url;
    });
    print("Download URL :$url");
    await addEvents(context);
    return url;
  }

  takePicture() async {
    print('Picker is called');
    File image = await ImagePicker.pickImage(source: ImageSource.camera);
//    File img = await ImagePicker.pickImage(source: ImageSource.gallery);
    if (image != null) {
      _image = image;
      setState(() {});
    }
  }

  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      if (image != null) {
        _image = image;
      }

      print('Image Path $_image');
    });
  }

  Future<void> addEvents(BuildContext context) async {
    // widget.ctrl.addEvent(
    //   userEmail,
    //   eventName.text,
    //   eventDescription.text,
    //   eventDate.text,
    //   teamName.text,
    //   imgUrl,
    // );

    await databaseReference.collection('pastEvents').document().setData(
      {
        'PostedBy': userEmail,
        'EventName': eventName.text,
        'EventDescription': eventDescription.text,
        'EventDate': eventDate.text,
        'TeamName': teamName.text,
        'EventImage': imgUrl,
        'test': FieldValue.arrayUnion([
          {"comment": "", "name": ""}
        ]),
      },
    );

    setState(() {
      isUploading = false;
    });

    _showAlert(context);
  }

  void _showAlert(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Your event added successfully !"),
          content: Text("Would you like to : "),
          actions: <Widget>[
            FlatButton(
              child: Text("Add another event"),
              onPressed: () {
                Navigator.of(context).pushReplacementNamed('/addCsp');
              },
            ),
            FlatButton(
                child: Text("Go to Home"),
                onPressed: () {
                  Navigator.of(context).pushReplacementNamed('/home');
                }),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(0xff, 241, 241, 254),
      appBar: AppBar(
        title: Text('Add CSP Experience'),
        backgroundColor: Colors.blue[800],
      ),
      body: Stack(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ListView(
              children: <Widget>[
                TextField(
                  controller: eventName,
                  decoration: InputDecoration(
                    hintText: "Event Name",
                    border: OutlineInputBorder(),
                  ),
                ),
                SizedBox(height: 15),
                TextField(
                  controller: eventDate,
                  decoration: InputDecoration(
                    hintText: "Event Date",
                    border: OutlineInputBorder(),
                  ),
                ),
                SizedBox(height: 15),
                TextField(
                  controller: teamName,
                  decoration: InputDecoration(
                    hintText: "Organized By",
                    border: OutlineInputBorder(),
                  ),
                ),
                SizedBox(height: 15),
                TextField(
                  maxLines: 10,
                  controller: eventDescription,
                  decoration: InputDecoration(
                    hintText: "Event Description",
                    border: OutlineInputBorder(),
                  ),
                ),
                SizedBox(height: 15),
                Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                      border: Border.all(width: 1, color: Colors.grey),
                      borderRadius: BorderRadius.circular(5)),
                  child: (_image != null)
                      ? Image.file(_image)
                      : Container(
                          child: Align(
                            alignment: Alignment.center,
                            child: Text("Choose or take a picture"),
                          ),
                        ),
                ),
                SizedBox(height: 15),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    InkWell(
                      onTap: () {
                        takePicture();
                      },
                      child: Container(
                        height: 50,
                        width: MediaQuery.of(context).size.width / 2.2,
                        decoration: BoxDecoration(
                            color: Colors.blue,
                            borderRadius: BorderRadius.circular(5)),
                        child: Center(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Icon(
                                Icons.photo_camera,
                                size: 30.0,
                              ),
                              Text(
                                'Open Camera',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20.0),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        getImage();
                      },
                      child: Container(
                          height: 50,
                          width: MediaQuery.of(context).size.width / 2.2,
                          decoration: BoxDecoration(
                              color: Colors.green,
                              borderRadius: BorderRadius.circular(5)),
                          child: Center(
                              child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Icon(
                                Icons.file_upload,
                                size: 30.0,
                              ),
                              Text(
                                'Open Gallery',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20.0),
                              ),
                            ],
                          ))),
                    )
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                InkWell(
                  onTap: () {
                    uploadPic(context);

                    setState(() {
                      isUploading = true;
                    });
                  },
                  child: Container(
                      height: 50,
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                          color: Colors.orange,
                          borderRadius: BorderRadius.circular(5)),
                      child: Center(
                          child: Text(
                        'Submit',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 20.0),
                      ))),
                ),
                SizedBox(
                  height: 10,
                ),
              ],
            ),
          ),
          isUploading
              ? Positioned.fill(
                  child: Container(
                    height: 100,
                    width: 100,
                    color: Colors.black.withOpacity(0.5),
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          CircularProgressIndicator(),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              "Uploading! Please Wait :)",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 20),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                )
              : Container()
        ],
      ),
    );
  }
}
