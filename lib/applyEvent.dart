import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class ApplyEvent extends StatefulWidget {
  String eventName;
  ApplyEvent({Key key, this.eventName}) : super(key: key);

  @override
  _ApplyEventState createState() => _ApplyEventState();
}

class _ApplyEventState extends State<ApplyEvent> {
  final userName = TextEditingController();
  final userNumber = TextEditingController();
  final userAddress = TextEditingController();
  final userEmail = TextEditingController();
  final userReason = TextEditingController();
  final databaseReference = Firestore.instance;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.eventName),
        backgroundColor: Colors.blue[800],
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListView(
          children: <Widget>[
            TextField(
              controller: userName,
              decoration: InputDecoration(
                hintText: "Name",
                border: OutlineInputBorder(),
              ),
            ),
            SizedBox(height: 15),
            TextField(
              controller: userAddress,
              decoration: InputDecoration(
                hintText: "Address",
                border: OutlineInputBorder(),
              ),
            ),
            SizedBox(height: 15),
            TextField(
              controller: userNumber,
              decoration: InputDecoration(
                hintText: "Phone Number",
                border: OutlineInputBorder(),
              ),
            ),
            SizedBox(height: 15),
            TextField(
              controller: userEmail,
              decoration: InputDecoration(
                hintText: "Email",
                border: OutlineInputBorder(),
              ),
            ),
            SizedBox(height: 15),
            TextField(
              maxLines: 8,
              controller: userReason,
              decoration: InputDecoration(
                hintText: "Why are you participating in this event?",
                border: OutlineInputBorder(),
              ),
            ),
            SizedBox(height: 15),
            InkWell(
              onTap: () {
                applyEvent();
                _neverSatisfied();

                userNumber.clear();
                userEmail.clear();
                userAddress.clear();
                userName.clear();
                userReason.clear();
              },
              child: Container(
                  height: 50,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                      color: Colors.red[800],
                      borderRadius: BorderRadius.circular(5)),
                  child: Center(
                      child: Text(
                    'APPLY',
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 20.0),
                  ))),
            ),
            SizedBox(
              height: 10,
            ),
          ],
        ),
      ),
    );
  }

  Future<void> applyEvent() async {
    await databaseReference.collection('applyEvents').document().setData({
      'Name': userName.text,
      'Address': userAddress.text,
      'Email': userEmail.text,
      'Number': userNumber.text,
      'Reason': userReason.text,
      'EventName': widget.eventName,
    });
  }

  Future<void> _neverSatisfied() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Successfull'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Thanks for applying!'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
                FocusScope.of(context).requestFocus(FocusNode());
              },
            ),
          ],
        );
      },
    );
  }
}
