import 'package:cloud_firestore/cloud_firestore.dart';

class Controller {
  final databaseReference = Firestore.instance;

  addUpcommingEvent(String userEmail, String eventName, String eventDescription,
      String eventDate, String teamName, String imgUrl) async {
    await databaseReference.collection('upcommingEvents').document().setData(
      {
        'PostedBy': userEmail,
        'EventName': eventName,
        'EventDescription': eventDescription,
        'EventDate': eventDate,
        'TeamName': teamName,
        'EventImage': imgUrl,
        'test': FieldValue.arrayUnion([
          {"comment": "", "name": ""}
        ]),
      },
    );
  }

  addIdeas(String userEmail, String eventName, String eventDescription,
      String eventDate, String teamName, String imgUrl) async {
    await databaseReference.collection('ideas').document().setData(
      {
        'PostedBy': userEmail,
        'EventName': eventName,
        'EventDescription': eventDescription,
        'EventDate': eventDate,
        'TeamName': teamName,
        'EventImage': imgUrl,
        'test': FieldValue.arrayUnion([
          {"comment": "", "name": ""}
        ]),
      },
    );
  }

  addPastEvent(String userEmail, String eventName, String eventDescription,
      String eventDate, String teamName, String imgUrl) async {
    await databaseReference.collection('pastEvents').document().setData(
      {
        'PostedBy': userEmail,
        'EventName': eventName,
        'EventDescription': eventDescription,
        'EventDate': eventDate,
        'TeamName': teamName,
        'EventImage': imgUrl,
        'test': FieldValue.arrayUnion([
          {"comment": "", "name": ""}
        ]),
      },
    );
  }
}
