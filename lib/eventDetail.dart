import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:csp/applyEvent.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'package:flutter/material.dart';

import 'widgets/custom_paint.dart';

class EventDetail extends StatefulWidget {
  String eventImage;
  String eventName;
  String eventDate;
  String teamName;
  String eventDescription;
  List comments;
  String id;
  String collectionName;

  EventDetail(
      {Key key,
      this.eventImage,
      this.eventDate,
      this.eventDescription,
      this.eventName,
      this.teamName,
      this.comments,
      this.collectionName,
      this.id})
      : super(key: key);

  @override
  _EventDetailState createState() => _EventDetailState();
}

class _EventDetailState extends State<EventDetail> {
  final commentController = TextEditingController();
  final databaseReference = Firestore.instance;
  String userEmail;
  @override
  void initState() {
    super.initState();
    userData();
  }

  Future<String> userData() async {
    final FirebaseUser user = await FirebaseAuth.instance.currentUser();
    final String email = user.email.toString();
    this.setState(() {
      userEmail = email;
    });
    print(userEmail);
    return email;
  }

  getEventDetail() {
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => EventDetail(
                eventImage: widget.eventImage,
                eventDate: widget.eventDate,
                eventDescription: widget.eventDescription,
                eventName: widget.eventName,
                teamName: widget.teamName,
                comments: widget.comments,
                id: widget.id,
                collectionName: widget.collectionName,
              )),
    );
  }

  @override
  Widget build(BuildContext context) {
    String id1 = widget.id;

    return Scaffold(
      backgroundColor: Color.fromARGB(0xff, 241, 241, 254),
      body: RefreshIndicator(
        onRefresh: () => getEventDetail(),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Stack(
                children: <Widget>[
                  CustomPaint(
                    painter: MyCustomPainter(),
                    child: Container(
                      height: 200.0,
                      child: Column(
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width,
                            margin: EdgeInsets.only(top: 40),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                InkWell(
                                    onTap: () {
                                      Navigator.pop(context);
                                    },
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Icon(
                                        Icons.arrow_back,
                                        color: Colors.white,
                                      ),
                                    )),
                                Flexible(
                                  child: Text(
                                    widget.eventName,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        boxShadow: [
                          new BoxShadow(
                            color: Color.fromRGBO(0, 0, 0, 0.08),
                            blurRadius: 20.0,
                          ),
                        ]),
                    margin: EdgeInsets.only(top: 100, left: 12, right: 12),
                    child: Image.network(
                      widget.eventImage,
                      fit: BoxFit.cover,
                      width: MediaQuery.of(context).size.width,
                      height: 250,
                    ),
                  ),
                ],
              ),
              Column(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.all(12.0),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10.0),
                        boxShadow: [
                          new BoxShadow(
                            color: Color.fromRGBO(0, 0, 0, 0.08),
                            blurRadius: 20.0,
                          ),
                        ]),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            widget.eventName,
                            style: TextStyle(
                              fontSize: 20.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Text(
                            widget.eventDate,
                            style: TextStyle(
                              fontSize: 15.0,
                              color: Colors.grey,
                            ),
                          ),
                          widget.teamName != null
                              ? Row(
                                  children: <Widget>[
                                    Text(
                                      'Conducted By: ',
                                      style: TextStyle(
                                          fontSize: 15.0,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Text(
                                      widget.teamName,
                                      style: TextStyle(
                                          fontSize: 15.0,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                )
                              : Container(),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            widget.eventDescription,
                            style: TextStyle(fontSize: 15.0),
                            textAlign: TextAlign.justify,
                          )
                        ],
                      ),
                    ),
                  ),
                  Container(
                      height: 350,
                      margin: EdgeInsets.all(12.0),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10.0),
                          boxShadow: [
                            new BoxShadow(
                              color: Color.fromRGBO(0, 0, 0, 0.08),
                              blurRadius: 20.0,
                            ),
                          ]),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: CachedNetworkImage(
                          fit: BoxFit.cover,
                          width: MediaQuery.of(context).size.width,
                          placeholder: (context, url) =>
                              CircularProgressIndicator(),
                          imageUrl:
                              'https://www.google.com/maps/d/u/0/thumbnail?mid=1-yyX5pvxUNjb3u2YlRq6oXbX1NE',
                        ),
                      )),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    margin: EdgeInsets.all(12.0),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10.0),
                        boxShadow: [
                          new BoxShadow(
                            color: Color.fromRGBO(0, 0, 0, 0.08),
                            blurRadius: 20.0,
                          ),
                        ]),
                    child: Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          widget.comments != null
                              ? Text(
                                  'Comment Section',
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                  ),
                                )
                              : Container(),
                          Divider(),
                          widget.comments != null
                              ? Column(
                                  children: widget.comments
                                      .asMap()
                                      .map((index, comment) => MapEntry(
                                          index,
                                          Padding(
                                              padding:
                                                  const EdgeInsets.all(0.0),
                                              child: ListTile(
                                                title: Text(
                                                  comment["name"],
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                ),
                                                subtitle:
                                                    Text(comment["comment"]),
                                              ))))
                                      .values
                                      .toList())
                              : Container(),
                          Divider(),
                          Container(
                            padding: EdgeInsets.only(top: 10),
                            width: MediaQuery.of(context).size.width,
                            child: Row(
                              children: <Widget>[
                                Container(
                                  height: 50,
                                  width:
                                      MediaQuery.of(context).size.width / 1.5,
                                  child: TextFormField(
                                    controller: commentController,
                                    cursorColor: Colors.black,
                                    decoration: new InputDecoration(
                                        hintText: "Add Comments",
                                        border: OutlineInputBorder()),
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                RaisedButton(
                                  child: Padding(
                                    padding: const EdgeInsets.all(6.0),
                                    child: Text("Posts"),
                                  ),
                                  onPressed: () async {
                                    print(commentController.text);

                                    await databaseReference
                                        .collection(widget.collectionName)
                                        .document(id1)
                                        .updateData({
                                      'test': FieldValue.arrayUnion([
                                        {
                                          "comment": commentController.text,
                                          "name": userEmail
                                        }
                                      ]),
                                    });
                                    Future.delayed(Duration.zero);
                                    commentController.clear();
                                    createRecord();
                                  },
                                  color: Colors.blue[800],
                                  textColor: Colors.white,
                                  padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                                  splashColor: Colors.grey,
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  widget.collectionName == "upcommingEvents"
                      ? InkWell(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ApplyEvent(
                                        eventName: widget.eventName,
                                      )),
                            );
                          },
                          child: Container(
                            height: 50,
                            margin: EdgeInsets.all(12),
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(
                                color: Colors.green[800],
                                borderRadius: BorderRadius.circular(5)),
                            child: Center(
                              child: Text(
                                'APPLY',
                                style: TextStyle(
                                    fontSize: 20,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    letterSpacing: 1),
                              ),
                            ),
                          ),
                        )
                      : widget.collectionName == "ideas"
                          ? InkWell(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => ApplyEvent(
                                            eventName: widget.eventName,
                                          )),
                                );
                              },
                              child: Container(
                                height: 50,
                                margin: EdgeInsets.all(12),
                                width: MediaQuery.of(context).size.width,
                                decoration: BoxDecoration(
                                    color: Colors.green[800],
                                    borderRadius: BorderRadius.circular(5)),
                                child: Center(
                                  child: Text(
                                    'APPLY',
                                    style: TextStyle(
                                        fontSize: 20,
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        letterSpacing: 1),
                                  ),
                                ),
                              ),
                            )
                          : Container()
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  void createRecord() async {
    // Navigator.push(
    //   context,
    //   MaterialPageRoute(
    //       builder: (context) => EventDetail(
    //             eventImage: widget.eventImage,
    //             eventDate: widget.eventDate,
    //             eventDescription: widget.eventDescription,
    //             eventName: widget.eventName,
    //             teamName: widget.teamName,
    //             comments: widget.comments,
    //             id: widget.id,
    //             collectionName: widget.collectionName,
    //           )),
    // );

    // DocumentReference ref = await databaseReference.collection("ideas").add({
    //   'Comments': commentController.text,
    // });
    //print(ref.documentID);
  }
}
