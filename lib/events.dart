import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:csp/eventDetail.dart';
import 'package:flutter/material.dart';

class Events extends StatefulWidget {
  String collectionName;
  Events({Key key, this.collectionName}) : super(key: key);

  @override
  _EventsState createState() => _EventsState();
}

class _EventsState extends State<Events> {
  @override
  Widget build(BuildContext context) {
    Widget _buildListItem(BuildContext context, DocumentSnapshot document) {
      return InkWell(
        onTap: () {
          print("ID" + document.documentID);
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => EventDetail(
                      eventImage: document['EventImage'],
                      eventDate: document['EventDate'],
                      eventDescription: document['EventDescription'],
                      eventName: document['EventName'],
                      teamName: document['TeamName'],
                      comments: document['test'],
                      id: document.documentID,
                      collectionName: widget.collectionName,
                    )),
          );
        },
        child: Container(
          margin: EdgeInsets.all(8),
          width: MediaQuery.of(context).size.width / 2.5,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10.0),
              boxShadow: [
                new BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.08),
                  blurRadius: 20.0,
                ),
              ]),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              document['EventImage'] != null
                  ? Image.network(
                      document['EventImage'],
                      height: MediaQuery.of(context).size.height / 3.5,
                      width: MediaQuery.of(context).size.width,
                      fit: BoxFit.cover,
                    )
                  : Container(),
              Spacer(),
              Padding(
                padding: const EdgeInsets.all(4.0),
                child: Text(
                  document['EventName'],
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                  ),
                ),
              ),
              Spacer(),
              Padding(
                padding: const EdgeInsets.all(4.0),
                child: Text(
                  document['EventDate'],
                  style: TextStyle(color: Colors.grey),
                ),
              ),
              Spacer(),
            ],
          ),
        ),
      );
    }

    return Scaffold(
      backgroundColor: Color.fromARGB(0xff, 241, 241, 254),
      appBar: AppBar(backgroundColor: Colors.blue[800], title: Text('Events')),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.all(8),
              height: MediaQuery.of(context).size.height,
              child: StreamBuilder(
                  stream: Firestore.instance
                      .collection(widget.collectionName)
                      .snapshots(),
                  builder: (context, snapshot) {
                    if (!snapshot.hasData)
                      return LinearProgressIndicator(
                        backgroundColor: Colors.green,
                      );
                    return ListView.builder(
                      itemExtent: 270,
                      itemCount: snapshot.data.documents.length,
                      itemBuilder: (context, index) => _buildListItem(
                          context, snapshot.data.documents[index]),
                    );
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
