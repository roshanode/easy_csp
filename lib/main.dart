import 'package:csp/AuthScreens/homeScreen.dart';
import 'package:csp/AuthScreens/login_signup_page.dart';
import 'package:csp/AuthScreens/root_page.dart';
import 'package:csp/addCSP.dart';
import 'package:csp/userProfile.dart';

import 'package:flutter/material.dart';

import 'AuthScreens/authentication.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: new RootPage(
        auth: new Auth(),
      ),
      routes: <String, WidgetBuilder>{
        '/logOut': (BuildContext context) => LoginSignUpPage(),
        '/profile': (BuildContext context) => UserProfile(),
        '/home': (BuildContext context) => HomeScreen(),
        '/addCsp': (BuildContext context) => AddCSP(),
        '/profile': (BuildContext context) => UserProfile(),
      },
    );
  }
}
