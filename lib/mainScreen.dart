// import 'package:csp/authentication.dart';
// import 'package:flutter/material.dart';

// import 'addCSP.dart';
// import 'homeScreen.dart';
// import 'userProfile.dart';

// class MainScreen extends StatefulWidget {
//   MainScreen({Key key, this.auth, this.userId, this.onSignedOut})
//       : super(key: key);

//   final BaseAuth auth;
//   final VoidCallback onSignedOut;
//   final String userId;
//   @override
//   _MainScreenState createState() => _MainScreenState();
// }

// class _MainScreenState extends State<MainScreen> {
//   int _selectedIndex = 0;
//   final _widgetOptions = [
//     HomeScreen(),
//     AddCSP(),
//     UserProfile(),
//   ];

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: _widgetOptions[_selectedIndex],
//       bottomNavigationBar: BottomNavigationBar(
//         items: <BottomNavigationBarItem>[
//           BottomNavigationBarItem(
//               icon: Icon(
//                 Icons.home,
//               ),
//               title: Text(
//                 'Home',
//               )),
//           BottomNavigationBarItem(
//               icon: Icon(
//                 Icons.add,
//               ),
//               title: Text(
//                 'Add CSP',
//               )),
//           BottomNavigationBarItem(
//               icon: Icon(
//                 Icons.account_box,
//               ),
//               title: Text(
//                 'Profile',
//               )),
//         ],
//         currentIndex: _selectedIndex,
//         unselectedItemColor: Color.fromARGB(0xff, 142, 142, 147),
//         fixedColor: Color.fromARGB(0xff, 26, 53, 98),
//         onTap: _onItemTapped,
//         type: BottomNavigationBarType.fixed,
//       ),
//     );
//   }

//   void _onItemTapped(int index) {
//     setState(() {
//       _selectedIndex = index;
//     });
//   }
// }
