import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:csp/messagetype.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class Message extends StatefulWidget {
  Message({Key key}) : super(key: key);

  @override
  _MessageState createState() => _MessageState();
}

class _MessageState extends State<Message> {
  String userEmail;

  @override
  void initState() {
    super.initState();
    userData();
  }

  Future<String> userData() async {
    final FirebaseUser user = await FirebaseAuth.instance.currentUser();
    final String email = user.email.toString();
    this.setState(() {
      userEmail = email;
    });
    print(userEmail);
    return email;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Message'),
        backgroundColor: Colors.blue[800],
      ),
      body: StreamBuilder(
          stream: Firestore.instance.collection('users').snapshots(),
          builder: (context, snapshot) {
            if (!snapshot.hasData)
              return LinearProgressIndicator(
                backgroundColor: Colors.green,
              );
            return ListView.builder(
              itemCount: snapshot.data.documents.length,
              itemBuilder: (BuildContext context, int index) {
                return InkWell(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => MessageType(
                                email: snapshot.data.documents[index]
                                    ['fullName'],
                              )),
                    );
                  },
                  child: Padding(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 8, vertical: 2),
                    child: Container(
                      child: snapshot.data.documents[index]['email'] ==
                              userEmail
                          ? Container()
                          : Card(
                              elevation: 3,
                              child: ListTile(
                                leading: Image.network(
                                    'https://cdn2.iconfinder.com/data/icons/people-80/96/Picture1-512.png'),
                                title: Text(
                                    snapshot.data.documents[index]['fullName']),
                              ),
                            ),
                    ),
                  ),
                );
              },
            );
          }),
    );
  }
}
