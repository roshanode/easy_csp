import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class MessageType extends StatefulWidget {
  String email;

  MessageType({Key key, this.email}) : super(key: key);

  @override
  _MessageTypeState createState() => _MessageTypeState();
}

class _MessageTypeState extends State<MessageType> {
  final messageController = TextEditingController();
  final databaseReference = Firestore.instance;
  List<String> message = [];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.email),
        backgroundColor: Colors.blue[800],
      ),
      body: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.bottomRight,
            child: ListView.builder(
                itemCount: message.length,
                itemBuilder: (BuildContext context, int index) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Container(
                          // alignment: Alignment.bottomRight,
                          margin: EdgeInsets.only(right: 15, top: 8),
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(50),
                            color: Colors.blue,
                          ),
                          child: Text(
                            message[index],
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          )),
                    ],
                  );
                }),
          ),
          Positioned(
            bottom: 0,
            child: Container(
              child: Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      height: 50,
                      width: MediaQuery.of(context).size.width / 1.5,
                      child: TextFormField(
                        controller: messageController,
                        cursorColor: Colors.black,
                        decoration: new InputDecoration(
                            hintText: "Type Something",
                            border: OutlineInputBorder()),
                      ),
                    ),
                  ),
                  SizedBox(width: 10),
                  RaisedButton(
                    child: Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: Row(
                        children: <Widget>[
                          Text(
                            "Send",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                            ),
                          ),
                          Icon(Icons.send)
                        ],
                      ),
                    ),
                    onPressed: () async {
                      message.add(messageController.text);
                      messageController.clear();
                      print("Message send" + message.toString());
                      FocusScope.of(context).requestFocus(FocusNode());

                      //   Future.delayed(Duration.zero);
                      //   createRecord();
                    },
                    color: Colors.blue[800],
                    textColor: Colors.white,
                    padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                    splashColor: Colors.grey,
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
