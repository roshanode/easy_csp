import 'package:cloud_firestore/cloud_firestore.dart';

class EventModel {
  final String eventName;
  final String eventDescription;
  final String eventDate;
  final String eventImage;
  final String teamName;
  final String postedBy;

  EventModel(
      {this.eventName,
      this.eventDescription,
      this.eventDate,
      this.eventImage,
      this.teamName,
      this.postedBy});

  factory EventModel.fromFirestore(DocumentSnapshot doc) {
    Map data = doc.data;
    return EventModel(
        eventName: data['EventName'],
        eventDate: data['EventDate'],
        eventDescription: data['EventDescription'],
        eventImage: data['EventImage'],
        teamName: data['TeamName'],
        postedBy: data['PostedBy']);
  }
}
