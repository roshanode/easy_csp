import 'package:cloud_firestore/cloud_firestore.dart';

class Profile {
  final String userName;
  final String userEmail;
  final String userImage;
  final String userNumber;

  Profile({this.userName, this.userEmail, this.userImage, this.userNumber});

  factory Profile.fromFirestore(DocumentSnapshot doc) {
    Map data = doc.data;
    return Profile(
      userName: data['name'],
      userEmail: data['email'],
      userImage: data['image'],
      userNumber: data['contact'],
    );
  }
}
