import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:csp/eventDetail.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';

class UserProfile extends StatefulWidget {
  @override
  _UserProfileState createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  String userEmail;
  bool isUploading = false;
  String imgUrl;
  File _image;
  final emailController = TextEditingController();
  final nameController = TextEditingController();
  final phoneController = TextEditingController();
  final databaseReference = Firestore.instance;
  @override
  void initState() {
    super.initState();
    userData();
  }

  Future<String> userData() async {
    final FirebaseUser user = await FirebaseAuth.instance.currentUser();
    final String email = user.email.toString();
    this.setState(() {
      userEmail = email;
    });
    print(userEmail);
    return email;
  }

  Future uploadPic(BuildContext context) async {
    String fileName = basename(_image.path);
    StorageReference firebaseStorageRef =
        FirebaseStorage.instance.ref().child(fileName);
    StorageUploadTask uploadTask = firebaseStorageRef.putFile(_image);
    var downUrl = await (await uploadTask.onComplete).ref.getDownloadURL();
    var url = downUrl.toString();
    setState(() {
      print("Profile Picture uploaded");
      imgUrl = url;
    });
    print("Download URL :$url");
    updateProfile(context);
    return url;
  }

  Future takePicture() async {
    print('Picker is called');
    File image = await ImagePicker.pickImage(source: ImageSource.camera);
//    File img = await ImagePicker.pickImage(source: ImageSource.gallery);
    if (image != null) {
      _image = image;
      setState(() {});
    }
  }

  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      if (image != null) {
        _image = image;
      }

      print('Image Path $_image');
    });
  }

  Future<void> updateProfile(BuildContext context) async {
    await databaseReference.collection('users').document(userEmail).updateData(
      {
        'image': imgUrl,
        'email': emailController.text,
        'fullName': nameController.text,
        'contact': phoneController.text
      },
    );

    setState(() {
      isUploading = false;
    });

    _showAlert(context);
  }

  void _chooseMode(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Choose picture form !"),
          actions: <Widget>[
            FlatButton(
              child: Text("Camera"),
              onPressed: () {
                takePicture();
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
                child: Text("Gallery"),
                onPressed: () {
                  getImage();
                  Navigator.of(context).pop();
                }),
          ],
        );
      },
    );
  }

  void _showAlert(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Your profile is updated successfully !"),
          actions: <Widget>[
            FlatButton(
                child: Text("ok"),
                onPressed: () {
                  Navigator.of(context).pushReplacementNamed('/profile');
                }),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    Widget _buildProfileListItem(BuildContext context, snapshot) {
      emailController.text = snapshot.data.data['email'];
      nameController.text = snapshot.data.data['fullName'];
      phoneController.text = snapshot.data.data['contact'];
      //for events
      Widget _buildListItem(BuildContext context, DocumentSnapshot document,
          String collectionName) {
        return InkWell(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => EventDetail(
                        eventImage: document['EventImage'],
                        eventDate: document['EventDate'],
                        eventDescription: document['EventDescription'],
                        eventName: document['EventName'],
                        teamName: document['TeamName'],
                        comments: document['test'],
                        id: document.documentID,
                        collectionName: collectionName,
                      )),
            );
          },
          child: Container(
            margin: EdgeInsets.all(8),
            width: MediaQuery.of(context).size.width / 2.5,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10.0),
                boxShadow: [
                  new BoxShadow(
                    color: Color.fromRGBO(0, 0, 0, 0.08),
                    blurRadius: 20.0,
                  ),
                ]),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                CachedNetworkImage(
                  imageUrl: document['EventImage'],
                  placeholder: (context, url) => CircularProgressIndicator(),
                  errorWidget: (context, url, error) => Icon(Icons.error),
                ),
                Spacer(),
                Text(
                  document['EventName'],
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                  ),
                ),
                Spacer(),
                Text(
                  document['EventDate'],
                  style: TextStyle(color: Colors.grey),
                ),
                Spacer()
              ],
            ),
          ),
        );
      }
      //for profile

      return Stack(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Stack(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 50),
                        child: (_image != null)
                            ? CircleAvatar(
                                radius: 80,
                                child: Image.file(
                                  _image,
                                  fit: BoxFit.contain,
                                ),
                              )
                            : (imgUrl != null)
                                ? CircleAvatar(
                                    backgroundColor: Colors.blueGrey,
                                    radius: 80,
                                    child: CachedNetworkImage(imageUrl: imgUrl))
                                : CachedNetworkImage(
                                    height: 100,
                                    width: 100,
                                    // width: MediaQuery.of(context).size.width,
                                    fit: BoxFit.cover,
                                    imageUrl:
                                        "https://i.pinimg.com/originals/83/c0/0f/83c00f59d66869aa22d3bd5f35e26c6d.png",
                                    placeholder: (context, url) =>
                                        CircularProgressIndicator(),
                                    errorWidget: (context, url, error) =>
                                        Icon(Icons.error),
                                  ),
                      ),
                      Positioned(
                        top: 150,
                        bottom: 0,
                        left: 100,
                        right: 0,
                        child: IconButton(
                          icon: Icon(Icons.camera),
                          color: Colors.white,
                          onPressed: () {
                            _chooseMode(context);
                          },
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      top: 20,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: TextFormField(
                            enabled: false,
                            controller: nameController,
                            decoration: InputDecoration(
                                border: OutlineInputBorder(),
                                labelText: 'Name'),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: TextFormField(
                              enabled: false,
                              controller: emailController,
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  labelText: 'Email')),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: TextFormField(
                              controller: phoneController,
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  labelText: 'Phone Number')),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: InkWell(
                            onTap: () {
                              uploadPic(context);
                              updateProfile(context);
                              setState(() {
                                isUploading = true;
                              });
                            },
                            child: Container(
                              height: 50,
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                  color: Colors.blue[800],
                                  borderRadius: BorderRadius.circular(10)),
                              child: Center(
                                child: Text(
                                  "UPDATE",
                                  style: TextStyle(
                                    fontSize: 20,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                'My Events',
                                style: TextStyle(fontSize: 20),
                              ),
                              Container(
                                height:
                                    MediaQuery.of(context).size.height / 3.5,
                                child: StreamBuilder(
                                    stream: Firestore.instance
                                        .collection('pastEvents')
                                        .snapshots(),
                                    builder: (context, snapshot) {
                                      if (!snapshot.hasData) {
                                        return LinearProgressIndicator(
                                          backgroundColor: Colors.green,
                                        );
                                      } else {
                                        var userPastEvents = [];
                                        for (int i = 0;
                                            i < snapshot.data.documents.length;
                                            i++) {
                                          if (snapshot.data.documents[i]
                                                  ["PostedBy"] ==
                                              userEmail) {
                                            userPastEvents.add(
                                                snapshot.data.documents[i]);
                                            print("Test" +
                                                snapshot.data.documents[i]
                                                    ["PostedBy"]);
                                          }
                                        }
                                        return ListView.builder(
                                          itemExtent: 200,
                                          scrollDirection: Axis.horizontal,
                                          itemCount: userPastEvents.length,
                                          itemBuilder: (context, index) =>
                                              _buildListItem(
                                                  context,
                                                  userPastEvents[index],
                                                  'pastEvents'),
                                        );
                                      }
                                    }),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      );
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue[800],
        title: Text("Profile Detail"),
        leading: IconButton(
          icon: Icon(Icons.chevron_left),
          onPressed: () {
            Navigator.of(context).pushReplacementNamed('/home');
          },
        ),
      ),
      body: Stack(
        children: <Widget>[
          SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: StreamBuilder(
                    stream: Firestore.instance
                        .collection('users')
                        .document(userEmail)
                        .snapshots(),
                    builder: (context, snapshot) {
                      if (!snapshot.hasData)
                        return LinearProgressIndicator(
                          backgroundColor: Colors.green,
                        );
                      return _buildProfileListItem(context, snapshot);
                    },
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
