import 'package:flutter/material.dart';

class MyCustomPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final height = size.height;
    final width = size.width;
    Paint paint = Paint();

    Path mainBackground = Path();
    mainBackground.addRect(Rect.fromLTRB(0, 0, width, height));
    paint.color = Colors.blue[800];
    canvas.drawPath(mainBackground, paint);

    Path purplePath = Path();
    purplePath.moveTo(width * 0.5, 0);
    purplePath.quadraticBezierTo(width * 0.5, 173, width, 173);

    purplePath.lineTo(width, 0);
    purplePath.close();
    paint.color = Colors.blue[700];
    canvas.drawPath(purplePath, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return oldDelegate != this;
  }
}
